#![feature(extern_types)]

use std::os::raw::*;

pub const TRUE: c_int = 1;
pub const FALSE: c_int = 0;
pub const RELEASE: c_int = 0;
pub const PRESS: c_int = 1;
pub const REPEAT: c_int = 2;
pub const HAT_CENTERED: c_int = 0;
pub const HAT_UP: c_int = 1;
pub const HAT_RIGHT: c_int = 2;
pub const HAT_DOWN: c_int = 4;
pub const HAT_LEFT: c_int = 8;
pub const HAT_RIGHT_UP: c_int = 3;
pub const HAT_RIGHT_DOWN: c_int = 6;
pub const HAT_LEFT_UP: c_int = 9;
pub const HAT_LEFT_DOWN: c_int = 12;
pub const KEY_UNKNOWN: i32 = -1;
pub const KEY_SPACE: c_int = 32;
pub const KEY_APOSTROPHE: c_int = 39;
pub const KEY_COMMA: c_int = 44;
pub const KEY_MINUS: c_int = 45;
pub const KEY_PERIOD: c_int = 46;
pub const KEY_SLASH: c_int = 47;
pub const KEY_0: c_int = 48;
pub const KEY_1: c_int = 49;
pub const KEY_2: c_int = 50;
pub const KEY_3: c_int = 51;
pub const KEY_4: c_int = 52;
pub const KEY_5: c_int = 53;
pub const KEY_6: c_int = 54;
pub const KEY_7: c_int = 55;
pub const KEY_8: c_int = 56;
pub const KEY_9: c_int = 57;
pub const KEY_SEMICOLON: c_int = 59;
pub const KEY_EQUAL: c_int = 61;
pub const KEY_A: c_int = 65;
pub const KEY_B: c_int = 66;
pub const KEY_C: c_int = 67;
pub const KEY_D: c_int = 68;
pub const KEY_E: c_int = 69;
pub const KEY_F: c_int = 70;
pub const KEY_G: c_int = 71;
pub const KEY_H: c_int = 72;
pub const KEY_I: c_int = 73;
pub const KEY_J: c_int = 74;
pub const KEY_K: c_int = 75;
pub const KEY_L: c_int = 76;
pub const KEY_M: c_int = 77;
pub const KEY_N: c_int = 78;
pub const KEY_O: c_int = 79;
pub const KEY_P: c_int = 80;
pub const KEY_Q: c_int = 81;
pub const KEY_R: c_int = 82;
pub const KEY_S: c_int = 83;
pub const KEY_T: c_int = 84;
pub const KEY_U: c_int = 85;
pub const KEY_V: c_int = 86;
pub const KEY_W: c_int = 87;
pub const KEY_X: c_int = 88;
pub const KEY_Y: c_int = 89;
pub const KEY_Z: c_int = 90;
pub const KEY_LEFT_BRACKET: c_int = 91;
pub const KEY_BACKSLASH: c_int = 92;
pub const KEY_RIGHT_BRACKET: c_int = 93;
pub const KEY_GRAVE_ACCENT: c_int = 96;
pub const KEY_WORLD_1: c_int = 161;
pub const KEY_WORLD_2: c_int = 162;
pub const KEY_ESCAPE: c_int = 256;
pub const KEY_ENTER: c_int = 257;
pub const KEY_TAB: c_int = 258;
pub const KEY_BACKSPACE: c_int = 259;
pub const KEY_INSERT: c_int = 260;
pub const KEY_DELETE: c_int = 261;
pub const KEY_RIGHT: c_int = 262;
pub const KEY_LEFT: c_int = 263;
pub const KEY_DOWN: c_int = 264;
pub const KEY_UP: c_int = 265;
pub const KEY_PAGE_UP: c_int = 266;
pub const KEY_PAGE_DOWN: c_int = 267;
pub const KEY_HOME: c_int = 268;
pub const KEY_END: c_int = 269;
pub const KEY_CAPS_LOCK: c_int = 280;
pub const KEY_SCROLL_LOCK: c_int = 281;
pub const KEY_NUM_LOCK: c_int = 282;
pub const KEY_PRINT_SCREEN: c_int = 283;
pub const KEY_PAUSE: c_int = 284;
pub const KEY_F1: c_int = 290;
pub const KEY_F2: c_int = 291;
pub const KEY_F3: c_int = 292;
pub const KEY_F4: c_int = 293;
pub const KEY_F5: c_int = 294;
pub const KEY_F6: c_int = 295;
pub const KEY_F7: c_int = 296;
pub const KEY_F8: c_int = 297;
pub const KEY_F9: c_int = 298;
pub const KEY_F10: c_int = 299;
pub const KEY_F11: c_int = 300;
pub const KEY_F12: c_int = 301;
pub const KEY_F13: c_int = 302;
pub const KEY_F14: c_int = 303;
pub const KEY_F15: c_int = 304;
pub const KEY_F16: c_int = 305;
pub const KEY_F17: c_int = 306;
pub const KEY_F18: c_int = 307;
pub const KEY_F19: c_int = 308;
pub const KEY_F20: c_int = 309;
pub const KEY_F21: c_int = 310;
pub const KEY_F22: c_int = 311;
pub const KEY_F23: c_int = 312;
pub const KEY_F24: c_int = 313;
pub const KEY_F25: c_int = 314;
pub const KEY_KP_0: c_int = 320;
pub const KEY_KP_1: c_int = 321;
pub const KEY_KP_2: c_int = 322;
pub const KEY_KP_3: c_int = 323;
pub const KEY_KP_4: c_int = 324;
pub const KEY_KP_5: c_int = 325;
pub const KEY_KP_6: c_int = 326;
pub const KEY_KP_7: c_int = 327;
pub const KEY_KP_8: c_int = 328;
pub const KEY_KP_9: c_int = 329;
pub const KEY_KP_DECIMAL: c_int = 330;
pub const KEY_KP_DIVIDE: c_int = 331;
pub const KEY_KP_MULTIPLY: c_int = 332;
pub const KEY_KP_SUBTRACT: c_int = 333;
pub const KEY_KP_ADD: c_int = 334;
pub const KEY_KP_ENTER: c_int = 335;
pub const KEY_KP_EQUAL: c_int = 336;
pub const KEY_LEFT_SHIFT: c_int = 340;
pub const KEY_LEFT_CONTROL: c_int = 341;
pub const KEY_LEFT_ALT: c_int = 342;
pub const KEY_LEFT_SUPER: c_int = 343;
pub const KEY_RIGHT_SHIFT: c_int = 344;
pub const KEY_RIGHT_CONTROL: c_int = 345;
pub const KEY_RIGHT_ALT: c_int = 346;
pub const KEY_RIGHT_SUPER: c_int = 347;
pub const KEY_MENU: c_int = 348;
pub const KEY_LAST: c_int = 348;
pub const MOD_SHIFT: c_int = 1;
pub const MOD_CONTROL: c_int = 2;
pub const MOD_ALT: c_int = 4;
pub const MOD_SUPER: c_int = 8;
pub const MOD_CAPS_LOCK: c_int = 16;
pub const MOD_NUM_LOCK: c_int = 32;
pub const MOUSE_BUTTON_1: c_int = 0;
pub const MOUSE_BUTTON_2: c_int = 1;
pub const MOUSE_BUTTON_3: c_int = 2;
pub const MOUSE_BUTTON_4: c_int = 3;
pub const MOUSE_BUTTON_5: c_int = 4;
pub const MOUSE_BUTTON_6: c_int = 5;
pub const MOUSE_BUTTON_7: c_int = 6;
pub const MOUSE_BUTTON_8: c_int = 7;
pub const MOUSE_BUTTON_LAST: c_int = 7;
pub const MOUSE_BUTTON_LEFT: c_int = 0;
pub const MOUSE_BUTTON_RIGHT: c_int = 1;
pub const MOUSE_BUTTON_MIDDLE: c_int = 2;
pub const JOYSTICK_1: c_int = 0;
pub const JOYSTICK_2: c_int = 1;
pub const JOYSTICK_3: c_int = 2;
pub const JOYSTICK_4: c_int = 3;
pub const JOYSTICK_5: c_int = 4;
pub const JOYSTICK_6: c_int = 5;
pub const JOYSTICK_7: c_int = 6;
pub const JOYSTICK_8: c_int = 7;
pub const JOYSTICK_9: c_int = 8;
pub const JOYSTICK_10: c_int = 9;
pub const JOYSTICK_11: c_int = 10;
pub const JOYSTICK_12: c_int = 11;
pub const JOYSTICK_13: c_int = 12;
pub const JOYSTICK_14: c_int = 13;
pub const JOYSTICK_15: c_int = 14;
pub const JOYSTICK_16: c_int = 15;
pub const JOYSTICK_LAST: c_int = 15;
pub const GAMEPAD_BUTTON_A: c_int = 0;
pub const GAMEPAD_BUTTON_B: c_int = 1;
pub const GAMEPAD_BUTTON_X: c_int = 2;
pub const GAMEPAD_BUTTON_Y: c_int = 3;
pub const GAMEPAD_BUTTON_LEFT_BUMPER: c_int = 4;
pub const GAMEPAD_BUTTON_RIGHT_BUMPER: c_int = 5;
pub const GAMEPAD_BUTTON_BACK: c_int = 6;
pub const GAMEPAD_BUTTON_START: c_int = 7;
pub const GAMEPAD_BUTTON_GUIDE: c_int = 8;
pub const GAMEPAD_BUTTON_LEFT_THUMB: c_int = 9;
pub const GAMEPAD_BUTTON_RIGHT_THUMB: c_int = 10;
pub const GAMEPAD_BUTTON_DPAD_UP: c_int = 11;
pub const GAMEPAD_BUTTON_DPAD_RIGHT: c_int = 12;
pub const GAMEPAD_BUTTON_DPAD_DOWN: c_int = 13;
pub const GAMEPAD_BUTTON_DPAD_LEFT: c_int = 14;
pub const GAMEPAD_BUTTON_LAST: c_int = 14;
pub const GAMEPAD_BUTTON_CROSS: c_int = 0;
pub const GAMEPAD_BUTTON_CIRCLE: c_int = 1;
pub const GAMEPAD_BUTTON_SQUARE: c_int = 2;
pub const GAMEPAD_BUTTON_TRIANGLE: c_int = 3;
pub const GAMEPAD_AXIS_LEFT_X: c_int = 0;
pub const GAMEPAD_AXIS_LEFT_Y: c_int = 1;
pub const GAMEPAD_AXIS_RIGHT_X: c_int = 2;
pub const GAMEPAD_AXIS_RIGHT_Y: c_int = 3;
pub const GAMEPAD_AXIS_LEFT_TRIGGER: c_int = 4;
pub const GAMEPAD_AXIS_RIGHT_TRIGGER: c_int = 5;
pub const GAMEPAD_AXIS_LAST: c_int = 5;
pub const NO_ERROR: c_int = 0;
pub const NOT_INITIALIZED: c_int = 65537;
pub const NO_CURRENT_CONTEXT: c_int = 65538;
pub const INVALID_ENUM: c_int = 65539;
pub const INVALID_VALUE: c_int = 65540;
pub const OUT_OF_MEMORY: c_int = 65541;
pub const API_UNAVAILABLE: c_int = 65542;
pub const VERSION_UNAVAILABLE: c_int = 65543;
pub const PLATFORM_ERROR: c_int = 65544;
pub const FORMAT_UNAVAILABLE: c_int = 65545;
pub const NO_WINDOW_CONTEXT: c_int = 65546;
pub const FOCUSED: c_int = 131073;
pub const ICONIFIED: c_int = 131074;
pub const RESIZABLE: c_int = 131075;
pub const VISIBLE: c_int = 131076;
pub const DECORATED: c_int = 131077;
pub const AUTO_ICONIFY: c_int = 131078;
pub const FLOATING: c_int = 131079;
pub const MAXIMIZED: c_int = 131080;
pub const CENTER_CURSOR: c_int = 131081;
pub const TRANSPARENT_FRAMEBUFFER: c_int = 131082;
pub const HOVERED: c_int = 131083;
pub const FOCUS_ON_SHOW: c_int = 131084;
pub const RED_BITS: c_int = 135169;
pub const GREEN_BITS: c_int = 135170;
pub const BLUE_BITS: c_int = 135171;
pub const ALPHA_BITS: c_int = 135172;
pub const DEPTH_BITS: c_int = 135173;
pub const STENCIL_BITS: c_int = 135174;
pub const ACCUM_RED_BITS: c_int = 135175;
pub const ACCUM_GREEN_BITS: c_int = 135176;
pub const ACCUM_BLUE_BITS: c_int = 135177;
pub const ACCUM_ALPHA_BITS: c_int = 135178;
pub const AUX_BUFFERS: c_int = 135179;
pub const STEREO: c_int = 135180;
pub const SAMPLES: c_int = 135181;
pub const SRGB_CAPABLE: c_int = 135182;
pub const REFRESH_RATE: c_int = 135183;
pub const DOUBLEBUFFER: c_int = 135184;
pub const CLIENT_API: c_int = 139265;
pub const CONTEXT_VERSION_MAJOR: c_int = 139266;
pub const CONTEXT_VERSION_MINOR: c_int = 139267;
pub const CONTEXT_REVISION: c_int = 139268;
pub const CONTEXT_ROBUSTNESS: c_int = 139269;
pub const OPENGL_FORWARD_COMPAT: c_int = 139270;
pub const OPENGL_DEBUG_CONTEXT: c_int = 139271;
pub const OPENGL_PROFILE: c_int = 139272;
pub const CONTEXT_RELEASE_BEHAVIOR: c_int = 139273;
pub const CONTEXT_NO_ERROR: c_int = 139274;
pub const CONTEXT_CREATION_API: c_int = 139275;
pub const SCALE_TO_MONITOR: c_int = 139276;
pub const COCOA_RETINA_FRAMEBUFFER: c_int = 143361;
pub const COCOA_FRAME_NAME: c_int = 143362;
pub const COCOA_GRAPHICS_SWITCHING: c_int = 143363;
pub const X11_CLASS_NAME: c_int = 147457;
pub const X11_INSTANCE_NAME: c_int = 147458;
pub const NO_API: c_int = 0;
pub const OPENGL_API: c_int = 196609;
pub const OPENGL_ES_API: c_int = 196610;
pub const NO_ROBUSTNESS: c_int = 0;
pub const NO_RESET_NOTIFICATION: c_int = 200705;
pub const LOSE_CONTEXT_ON_RESET: c_int = 200706;
pub const OPENGL_ANY_PROFILE: c_int = 0;
pub const OPENGL_CORE_PROFILE: c_int = 204801;
pub const OPENGL_COMPAT_PROFILE: c_int = 204802;
pub const CURSOR: c_int = 208897;
pub const STICKY_KEYS: c_int = 208898;
pub const STICKY_MOUSE_BUTTONS: c_int = 208899;
pub const LOCK_KEY_MODS: c_int = 208900;
pub const CURSOR_NORMAL: c_int = 212993;
pub const CURSOR_HIDDEN: c_int = 212994;
pub const CURSOR_DISABLED: c_int = 212995;
pub const ANY_RELEASE_BEHAVIOR: c_int = 0;
pub const RELEASE_BEHAVIOR_FLUSH: c_int = 217089;
pub const RELEASE_BEHAVIOR_NONE: c_int = 217090;
pub const NATIVE_CONTEXT_API: c_int = 221185;
pub const EGL_CONTEXT_API: c_int = 221186;
pub const OSMESA_CONTEXT_API: c_int = 221187;
pub const ARROW_CURSOR: c_int = 221185;
pub const IBEAM_CURSOR: c_int = 221186;
pub const CROSSHAIR_CURSOR: c_int = 221187;
pub const HAND_CURSOR: c_int = 221188;
pub const HRESIZE_CURSOR: c_int = 221189;
pub const VRESIZE_CURSOR: c_int = 221190;
pub const CONNECTED: c_int = 262145;
pub const DISCONNECTED: c_int = 262146;
pub const JOYSTICK_HAT_BUTTONS: c_int = 327681;
pub const COCOA_CHDIR_RESOURCES: c_int = 331777;
pub const COCOA_MENUBAR: c_int = 331778;
pub const DONT_CARE: c_int = -1;

pub type GlProc = Option<unsafe extern "C" fn()>;
pub type VkProc = Option<unsafe extern "C" fn()>;

extern {
    pub type VkInstanceT;
    pub type VkPhysicalDeviceT;
    pub type VkAllocationCallbacks;
}

pub type VkResult = i32;
pub type VkInstance = *const VkInstanceT;
pub type VkPhysicalDevice = *const VkPhysicalDeviceT;
pub type VkSurfaceKHR = u64;

extern {
    pub type Monitor;
    pub type Window;
    pub type Cursor;
}

pub type ErrorFun = Option<
    unsafe extern "C" fn(error: c_int, description: *const c_char),
>;
pub type WindowPosFun = Option<
    unsafe extern "C" fn(window: *mut Window, xpos: c_int, ypos: c_int),
>;
pub type WindowSizeFun = Option<
    unsafe extern "C" fn(window: *mut Window, width: c_int, height: c_int),
>;
pub type WindowCloseFun = Option<
    unsafe extern "C" fn(window: *mut Window),
>;
pub type WindowRefreshFun = Option<
    unsafe extern "C" fn(window: *mut Window),
>;
pub type WindowFocusFun = Option<
    unsafe extern "C" fn(window: *mut Window, focused: c_int),
>;
pub type WindowIconifyFun = Option<
    unsafe extern "C" fn(window: *mut Window, iconified: c_int),
>;
pub type WindowMaximizeFun = Option<
    unsafe extern "C" fn(window: *mut Window, maximized: c_int),
>;
pub type FramebufferSizeFun = Option<
    unsafe extern "C" fn(window: *mut Window, width: c_int, height: c_int),
>;
pub type WindowContentScaleFun = Option<
    unsafe extern "C" fn(window: *mut Window, xscale: f32, yscale: f32),
>;
pub type MouseButtonFun = Option<unsafe extern "C" fn(
    window: *mut Window,
    button: c_int,
    action: c_int,
    mods: c_int,
)>;
pub type CursorPosFun = Option<
    unsafe extern "C" fn(window: *mut Window, xpos: f64, ypos: f64),
>;
pub type CursorEnterFun = Option<
    unsafe extern "C" fn(window: *mut Window, entered: c_int),
>;
pub type ScrollFun = Option<
    unsafe extern "C" fn(window: *mut Window, xoffset: f64, yoffset: f64),
>;
pub type KeyFun = Option<unsafe extern "C" fn(
    window: *mut Window,
    key: c_int,
    scancode: c_int,
    action: c_int,
    mods: c_int,
)>;
pub type CharFun = Option<
    unsafe extern "C" fn(window: *mut Window, codepoint: c_uint),
>;
pub type CharModsFun = Option<
    unsafe extern "C" fn(window: *mut Window, codepoint: c_uint, mods: c_int),
>;
pub type DropFun = Option<unsafe extern "C" fn(
    window: *mut Window,
    count: c_int,
    paths: *mut *const c_char,
)>;
pub type MonitorFun = Option<
    unsafe extern "C" fn(monitor: *mut Monitor, event: c_int),
>;
pub type JoystickFun = Option<
    unsafe extern "C" fn(jid: c_int, event: c_int),
>;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct VidMode {
    pub width: c_int,
    pub height: c_int,
    pub red_bits: c_int,
    pub green_bits: c_int,
    pub blue_bits: c_int,
    pub refresh_rate: c_int,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct GammaRamp {
    pub red: *mut c_ushort,
    pub green: *mut c_ushort,
    pub blue: *mut c_ushort,
    pub size: c_uint,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Image {
    pub width: c_int,
    pub height: c_int,
    pub pixels: *mut c_uchar,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct GamepadState {
    pub buttons: [c_uchar; 15],
    pub axes: [f32; 6],
}

#[link(name = "glfw")]
extern "C" {
    #[link_name = "glfwInit"]
    pub fn init() -> c_int;
    #[link_name = "glfwTerminate"]
    pub fn terminate();
    #[link_name = "glfwInitHint"]
    pub fn init_hint(hint: c_int, value: c_int);
    #[link_name = "glfwGetVersion"]
    pub fn get_version(major: *mut c_int, minor: *mut c_int, rev: *mut c_int);
    #[link_name = "glfwGetVersionString"]
    pub fn get_version_string() -> *const c_char;
    #[link_name = "glfwGetError"]
    pub fn get_error(description: *mut *const c_char) -> c_int;
    #[link_name = "glfwSetErrorCallback"]
    pub fn set_error_callback(cbfun: ErrorFun) -> ErrorFun;
    #[link_name = "glfwGetMonitors"]
    pub fn get_monitors(count: *mut c_int) -> *mut *mut Monitor;
    #[link_name = "glfwGetPrimaryMonitor"]
    pub fn get_primary_monitor() -> *mut Monitor;
    #[link_name = "glfwGetMonitorPos"]
    pub fn get_monitor_pos(
        monitor: *mut Monitor,
        xpos: *mut c_int,
        ypos: *mut c_int,
    );
    #[link_name = "glfwGetMonitorPhysicalSize"]
    pub fn get_monitor_physical_size(
        monitor: *mut Monitor,
        width_mm: *mut c_int,
        height_mm: *mut c_int,
    );
    #[link_name = "glfwGetMonitorContentScale"]
    pub fn get_monitor_content_scale(
        monitor: *mut Monitor,
        xscale: *mut f32,
        yscale: *mut f32,
    );
    #[link_name = "glfwGetMonitorName"]
    pub fn get_monitor_name(monitor: *mut Monitor) -> *const c_char;
    #[link_name = "glfwSetMonitorUserPointer"]
    pub fn set_monitor_user_pointer(
        monitor: *mut Monitor,
        pointer: *mut c_void,
    );
    #[link_name = "glfwGetMonitorUserPointer"]
    pub fn get_monitor_user_pointer(monitor: *mut Monitor) -> *mut c_void;
    #[link_name = "glfwSetMonitorCallback"]
    pub fn set_monitor_callback(cbfun: MonitorFun) -> MonitorFun;
    #[link_name = "glfwGetVideoModes"]
    pub fn get_video_modes(monitor: *mut Monitor, count: *mut c_int)
        -> *const VidMode;
    #[link_name = "glfwGetVideoMode"]
    pub fn get_video_mode(monitor: *mut Monitor) -> *const VidMode;
    #[link_name = "glfwSetGamma"]
    pub fn set_gamma(monitor: *mut Monitor, gamma: f32);
    #[link_name = "glfwGetGammaRamp"]
    pub fn get_gamma_ramp(monitor: *mut Monitor) -> *const GammaRamp;
    #[link_name = "glfwSetGammaRamp"]
    pub fn set_gamma_ramp(monitor: *mut Monitor, ramp: *const GammaRamp);
    #[link_name = "glfwDefaultWindowHints"]
    pub fn default_window_hints();
    #[link_name = "glfwWindowHint"]
    pub fn window_hint(hint: c_int, value: c_int);
    #[link_name = "glfwWindowHintString"]
    pub fn window_hint_string(hint: c_int, value: *const c_char);
    #[link_name = "glfwCreateWindow"]
    pub fn create_window(
        width: c_int,
        height: c_int,
        title: *const c_char,
        monitor: *mut Monitor,
        share: *mut Window,
    ) -> *mut Window;
    #[link_name = "glfwDestroyWindow"]
    pub fn destroy_window(window: *mut Window);
    #[link_name = "glfwWindowShouldClose"]
    pub fn window_should_close(window: *mut Window) -> c_int;
    #[link_name = "glfwSetWindowShouldClose"]
    pub fn set_window_should_close(window: *mut Window, value: c_int);
    #[link_name = "glfwSetWindowTitle"]
    pub fn set_window_title(window: *mut Window, title: *const c_char);
    #[link_name = "glfwSetWindowIcon"]
    pub fn set_window_icon(
        window: *mut Window,
        count: c_int,
        images: *const Image,
    );
    #[link_name = "glfwGetWindowPos"]
    pub fn get_window_pos(
        window: *mut Window,
        xpos: *mut c_int,
        ypos: *mut c_int,
    );
    #[link_name = "glfwSetWindowPos"]
    pub fn set_window_pos(
        window: *mut Window,
        xpos: c_int,
        ypos: c_int,
    );
    #[link_name = "glfwGetWindowSize"]
    pub fn get_window_size(
        window: *mut Window,
        width: *mut c_int,
        height: *mut c_int,
    );
    #[link_name = "glfwSetWindowSizeLimits"]
    pub fn set_window_size_limits(
        window: *mut Window,
        minwidth: c_int,
        minheight: c_int,
        maxwidth: c_int,
        maxheight: c_int,
    );
    #[link_name = "glfwSetWindowAspectRatio"]
    pub fn set_window_aspect_ratio(
        window: *mut Window,
        numer: c_int,
        denom: c_int,
    );
    #[link_name = "glfwSetWindowSize"]
    pub fn set_window_size(
        window: *mut Window,
        width: c_int,
        height: c_int,
    );
    #[link_name = "glfwGetFramebufferSize"]
    pub fn get_framebuffer_size(
        window: *mut Window,
        width: *mut c_int,
        height: *mut c_int,
    );
    #[link_name = "glfwGetWindowFrameSize"]
    pub fn get_window_frame_size(
        window: *mut Window,
        left: *mut c_int,
        top: *mut c_int,
        right: *mut c_int,
        bottom: *mut c_int,
    );
    #[link_name = "glfwGetWindowContentScale"]
    pub fn get_window_content_scale(
        window: *mut Window,
        xscale: *mut f32,
        yscale: *mut f32,
    );
    #[link_name = "glfwGetWindowOpacity"]
    pub fn get_window_opacity(window: *mut Window) -> f32;
    #[link_name = "glfwSetWindowOpacity"]
    pub fn set_window_opacity(window: *mut Window, opacity: f32);
    #[link_name = "glfwIconifyWindow"]
    pub fn iconify_window(window: *mut Window);
    #[link_name = "glfwRestoreWindow"]
    pub fn restore_window(window: *mut Window);
    #[link_name = "glfwMaximizeWindow"]
    pub fn maximize_window(window: *mut Window);
    #[link_name = "glfwShowWindow"]
    pub fn show_window(window: *mut Window);
    #[link_name = "glfwHideWindow"]
    pub fn hide_window(window: *mut Window);
    #[link_name = "glfwFocusWindow"]
    pub fn focus_window(window: *mut Window);
    #[link_name = "glfwRequestWindowAttention"]
    pub fn request_window_attention(window: *mut Window);
    #[link_name = "glfwGetWindowMonitor"]
    pub fn get_window_monitor(window: *mut Window) -> *mut Monitor;
    #[link_name = "glfwSetWindowMonitor"]
    pub fn set_window_monitor(
        window: *mut Window,
        monitor: *mut Monitor,
        xpos: c_int,
        ypos: c_int,
        width: c_int,
        height: c_int,
        refresh_rate: c_int,
    );
    #[link_name = "glfwGetWindowAttrib"]
    pub fn get_window_attrib(window: *mut Window, attrib: c_int) -> c_int;
    #[link_name = "glfwSetWindowAttrib"]
    pub fn set_window_attrib(window: *mut Window, attrib: c_int, value: c_int);
    #[link_name = "glfwSetWindowUserPointer"]
    pub fn set_window_user_pointer(window: *mut Window, pointer: *mut c_void);
    #[link_name = "glfwGetWindowUserPointer"]
    pub fn get_window_user_pointer(window: *mut Window) -> *mut c_void;
    #[link_name = "glfwSetWindowPosCallback"]
    pub fn set_window_pos_callback(
        window: *mut Window,
        cbfun: WindowPosFun,
    ) -> WindowPosFun;
    #[link_name = "glfwSetWindowSizeCallback"]
    pub fn set_window_size_callback(
        window: *mut Window,
        cbfun: WindowSizeFun,
    ) -> WindowSizeFun;
    #[link_name = "glfwSetWindowCloseCallback"]
    pub fn set_window_close_callback(
        window: *mut Window,
        cbfun: WindowCloseFun,
    ) -> WindowCloseFun;
    #[link_name = "glfwSetWindowRefreshCallback"]
    pub fn set_window_refresh_callback(
        window: *mut Window,
        cbfun: WindowRefreshFun,
    ) -> WindowRefreshFun;
    #[link_name = "glfwSetWindowFocusCallback"]
    pub fn set_window_focus_callback(
        window: *mut Window,
        cbfun: WindowFocusFun,
    ) -> WindowFocusFun;
    #[link_name = "glfwSetWindowIconifyCallback"]
    pub fn set_window_iconify_callback(
        window: *mut Window,
        cbfun: WindowIconifyFun,
    ) -> WindowIconifyFun;
    #[link_name = "glfwSetWindowMaximizeCallback"]
    pub fn set_window_maximize_callback(
        window: *mut Window,
        cbfun: WindowMaximizeFun,
    ) -> WindowMaximizeFun;
    #[link_name = "glfwSetFramebufferSizeCallback"]
    pub fn set_framebuffer_size_callback(
        window: *mut Window,
        cbfun: FramebufferSizeFun,
    ) -> FramebufferSizeFun;
    #[link_name = "glfwSetWindowContentScaleCallback"]
    pub fn set_window_content_scale_callback(
        window: *mut Window,
        cbfun: WindowContentScaleFun,
    ) -> WindowContentScaleFun;
    #[link_name = "glfwPollEvents"]
    pub fn poll_events();
    #[link_name = "glfwWaitEvents"]
    pub fn wait_events();
    #[link_name = "glfwWaitEventsTimeout"]
    pub fn wait_events_timeout(timeout: f64);
    #[link_name = "glfwPostEmptyEvent"]
    pub fn post_empty_event();
    #[link_name = "glfwGetInputMode"]
    pub fn get_input_mode(window: *mut Window, mode: c_int) -> c_int;
    #[link_name = "glfwSetInputMode"]
    pub fn set_input_mode(window: *mut Window, mode: c_int, value: c_int);
    #[link_name = "glfwGetKeyName"]
    pub fn get_key_name(key: c_int, scancode: c_int) -> *const c_char;
    #[link_name = "glfwGetKeyScancode"]
    pub fn get_key_scancode(key: c_int) -> c_int;
    #[link_name = "glfwGetKey"]
    pub fn get_key(window: *mut Window, key: c_int) -> c_int;
    #[link_name = "glfwGetMouseButton"]
    pub fn get_mouse_button(window: *mut Window, button: c_int) -> c_int;
    #[link_name = "glfwGetCursorPos"]
    pub fn get_cursor_pos(
        window: *mut Window,
        xpos: *mut f64,
        ypos: *mut f64,
    );
    #[link_name = "glfwSetCursorPos"]
    pub fn set_cursor_pos(window: *mut Window, xpos: f64, ypos: f64);
    #[link_name = "glfwCreateCursor"]
    pub fn create_cursor(image: *const Image, xhot: c_int, yhot: c_int)
        -> *mut Cursor;
    #[link_name = "glfwCreateStandardCursor"]
    pub fn create_standard_cursor(shape: c_int) -> *mut Cursor;
    #[link_name = "glfwDestroyCursor"]
    pub fn destroy_cursor(cursor: *mut Cursor);
    #[link_name = "glfwSetCursor"]
    pub fn set_cursor(window: *mut Window, cursor: *mut Cursor);
    #[link_name = "glfwSetKeyCallback"]
    pub fn set_key_callback(
        window: *mut Window,
        cbfun: KeyFun,
    ) -> KeyFun;
    #[link_name = "glfwSetCharCallback"]
    pub fn set_char_callback(
        window: *mut Window,
        cbfun: CharFun,
    ) -> CharFun;
    #[link_name = "glfwSetCharModsCallback"]
    pub fn set_char_mods_callback(
        window: *mut Window,
        cbfun: CharModsFun,
    ) -> CharModsFun;
    #[link_name = "glfwSetMouseButtonCallback"]
    pub fn set_mouse_button_callback(
        window: *mut Window,
        cbfun: MouseButtonFun,
    ) -> MouseButtonFun;
    #[link_name = "glfwSetCursorPosCallback"]
    pub fn set_cursor_pos_callback(
        window: *mut Window,
        cbfun: CursorPosFun,
    ) -> CursorPosFun;
    #[link_name = "glfwSetCursorEnterCallback"]
    pub fn set_cursor_enter_callback(
        window: *mut Window,
        cbfun: CursorEnterFun,
    ) -> CursorEnterFun;
    #[link_name = "glfwSetScrollCallback"]
    pub fn set_scroll_callback(
        window: *mut Window,
        cbfun: ScrollFun,
    ) -> ScrollFun;
    #[link_name = "glfwSetDropCallback"]
    pub fn set_drop_callback(
        window: *mut Window,
        cbfun: DropFun,
    ) -> DropFun;
    #[link_name = "glfwJoystickPresent"]
    pub fn joystick_present(jid: c_int) -> c_int;
    #[link_name = "glfwGetJoystickAxes"]
    pub fn get_joystick_axes(jid: c_int, count: *mut c_int) -> *const f32;
    #[link_name = "glfwGetJoystickButtons"]
    pub fn get_joystick_buttons(jid: c_int, count: *mut c_int)
        -> *const c_uchar;
    #[link_name = "glfwGetJoystickHats"]
    pub fn get_joystick_hats(jid: c_int, count: *mut c_int) -> *const c_uchar;
    #[link_name = "glfwGetJoystickName"]
    pub fn get_joystick_name(jid: c_int) -> *const c_char;
    #[link_name = "glfwGetJoystickGUID"]
    pub fn get_joystick_guid(jid: c_int) -> *const c_char;
    #[link_name = "glfwSetJoystickUserPointer"]
    pub fn set_joystick_user_pointer(jid: c_int, pointer: *mut c_void);
    #[link_name = "glfwGetJoystickUserPointer"]
    pub fn get_joystick_user_pointer(jid: c_int) -> *mut c_void;
    #[link_name = "glfwJoystickIsGamepad"]
    pub fn joystick_is_gamepad(jid: c_int) -> c_int;
    #[link_name = "glfwSetJoystickCallback"]
    pub fn set_joystick_callback(cbfun: JoystickFun) -> JoystickFun;
    #[link_name = "glfwUpdateGamepadMappings"]
    pub fn update_gamepad_mappings(string: *const c_char) -> c_int;
    #[link_name = "glfwGetGamepadName"]
    pub fn get_gamepad_name(jid: c_int) -> *const c_char;
    #[link_name = "glfwGetGamepadState"]
    pub fn get_gamepad_state(jid: c_int, state: *mut GamepadState) -> c_int;
    #[link_name = "glfwSetClipboardString"]
    pub fn set_clipboard_string(window: *mut Window, string: *const c_char);
    #[link_name = "glfwGetClipboardString"]
    pub fn get_clipboard_string(window: *mut Window) -> *const c_char;
    #[link_name = "glfwGetTime"]
    pub fn get_time() -> f64;
    #[link_name = "glfwSetTime"]
    pub fn set_time(time: f64);
    #[link_name = "glfwGetTimerValue"]
    pub fn get_timer_value() -> u64;
    #[link_name = "glfwGetTimerFrequency"]
    pub fn get_timer_frequency() -> u64;
    #[link_name = "glfwMakeContextCurrent"]
    pub fn make_context_current(window: *mut Window);
    #[link_name = "glfwGetCurrentContext"]
    pub fn get_current_context() -> *mut Window;
    #[link_name = "glfwSwapBuffers"]
    pub fn swap_buffers(window: *mut Window);
    #[link_name = "glfwSwapInterval"]
    pub fn swap_interval(interval: c_int);
    #[link_name = "glfwExtensionSupported"]
    pub fn extension_supported(extension: *const c_char) -> c_int;
    #[link_name = "glfwGetProcAddress"]
    pub fn get_proc_address(procname: *const c_char) -> GlProc;
    #[link_name = "glfwVulkanSupported"]
    pub fn vulkan_supported() -> c_int;
    #[link_name = "glfwGetRequiredInstanceExtensions"]
    pub fn get_required_instance_extensions(count: *mut u32)
        -> *mut *const c_char;
    #[link_name = "glfwGetInstanceProcAddress"]
    pub fn get_instance_proc_address(
        instance: VkInstance,
        procname: *const c_char,
    ) -> VkProc;
    #[link_name = "glfwGetPhysicalDevicePresentationSupport"]
    pub fn get_physical_device_presentation_support(
        instance: VkInstance,
        device: VkPhysicalDevice,
        queuefamily: u32,
    ) -> c_int;
    #[link_name = "glfwCreateWindowSurface"]
    pub fn create_window_surface(
        instance: VkInstance,
        window: *mut Window,
        allocator: *const VkAllocationCallbacks,
        surface: *mut VkSurfaceKHR,
    ) -> VkResult;
}
