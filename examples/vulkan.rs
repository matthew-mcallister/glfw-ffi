//! This example demonstrates how to load Vulkan function pointers and
//! create a surface using GLFW.
#![feature(try_blocks)]

extern crate glfw_ffi as glfw;
extern crate vk_ffi as vk;
extern crate vk_ffi_loader as vkl;

use std::ptr;

macro_rules! c_str {
    ($str:expr) => { concat!($str, "\0").as_ptr() as *const _ as *const _ }
}

macro_rules! impl_print_bits {
    ($fn_name:ident($type:ty) { $($bit:expr => $name:expr,)* }) => {
        fn $fn_name(mask: $type) {
            let mut first = true;
            for &(bit, name) in [$(($bit, $name),)*].iter() {
                if !mask.intersects(bit) { continue; }
                if first { print!("{}", name); first = false; }
                else { print!(" | {}", name); }
            }
            if first { print!("0"); }
        }
    }
}

fn main() {
    unsafe { unsafe_main() }
}

unsafe fn unsafe_main() {
    if glfw::init() != glfw::TRUE {
        panic!("failed to initialize GLFW");
    }

    if glfw::vulkan_supported() != glfw::TRUE {
        panic!("Vulkan not supported by GLFW");
    }

    // Load Vulkan entry points
    let get_instance_proc_addr =
        std::mem::transmute(glfw::get_instance_proc_address as *const ());
    let entry = vkl::Entry::load(get_instance_proc_addr);

    // Get required extensions
    let mut num_exts: u32 = 0;
    let exts = glfw::get_required_instance_extensions(&mut num_exts as *mut _);

    // Create a Vulkan instance
    let app_info = vk::ApplicationInfo {
        s_type: vk::StructureType::APPLICATION_INFO,
        p_next: ptr::null(),
        p_application_name: c_str!("GLFW Vulkan demo"),
        application_version: vk::make_version!(0, 1, 0),
        p_engine_name: ptr::null(),
        engine_version: vk::make_version!(0, 1, 0),
        api_version: vk::API_VERSION_1_0,
    };
    let create_info = vk::InstanceCreateInfo {
        s_type: vk::StructureType::INSTANCE_CREATE_INFO,
        p_next: ptr::null(),
        flags: Default::default(),
        p_application_info: &app_info as *const _,
        enabled_layer_count: 0,
        pp_enabled_layer_names: ptr::null(),
        enabled_extension_count: num_exts,
        pp_enabled_extension_names: exts,
    };
    let mut instance = vk::null();
    entry.create_instance
        (&create_info as *const _, ptr::null(), &mut instance as *mut _)
        .check().unwrap();

    // Load instance functions
    let instance = vkl::InstanceTable::load(instance, get_instance_proc_addr);

    // Select a physical device with presentation support
    let physical_devices =
        vk::enumerate2!(instance, enumerate_physical_devices).unwrap();
    let physical_device = physical_devices.iter().cloned().find(|pdev| {
        // We could check for presentation support on other queue
        // families, but that doesn't seem to be a problem in practice.
        glfw::TRUE == glfw::get_physical_device_presentation_support
            (instance.instance.0 as *const _, pdev.0 as *const _, 0)
    }).expect("no graphics device with presentation support");

    // Create logical device
    let queue_create_info = vk::DeviceQueueCreateInfo {
        s_type: vk::StructureType::DEVICE_QUEUE_CREATE_INFO,
        p_next: ptr::null(),
        flags: Default::default(),
        queue_family_index: 0,
        queue_count: 1,
        p_queue_priorities: &1.0f32 as *const _,
    };
    let features: vk::PhysicalDeviceFeatures = Default::default();
    let create_info = vk::DeviceCreateInfo {
        s_type: vk::StructureType::DEVICE_CREATE_INFO,
        p_next: ptr::null(),
        flags: Default::default(),
        queue_create_info_count: 1,
        p_queue_create_infos: &queue_create_info as *const _,
        enabled_layer_count: 0,
        pp_enabled_layer_names: ptr::null(),
        enabled_extension_count: 0,
        pp_enabled_extension_names: ptr::null(),
        p_enabled_features: &features as *const _,
    };
    let mut device = vk::null();
    instance.create_device(
        physical_device,
        &create_info as *const _,
        ptr::null(),
        &mut device as *mut _,
    ).check().unwrap();

    // Load device function pointers
    let get_device_proc_addr =
        std::mem::transmute(glfw::get_instance_proc_address(
            instance.instance.0 as *const _,
            c_str!("vkGetDeviceProcAddr"),
        ));
    let device = vkl::DeviceTable::load(device, get_device_proc_addr);

    // Create a window
    glfw::window_hint(glfw::CLIENT_API, glfw::NO_API);
    let window = glfw::create_window
        (640, 480, c_str!("Vulkan demo"), 0 as *mut _, 0 as *mut _);
    if window.is_null() { panic!("failed to create window"); }

    // Create a surface
    let mut surface = vk::null();
    let res = glfw::create_window_surface(
        instance.instance.0 as *const _,
        window,
        0 as *const _,
        &mut surface as *mut _ as *mut _,
    );
    vk::Result(res).check().unwrap();

    // Display some info about the surface so we know it worked
    let mut caps: vk::SurfaceCapabilitiesKhr = Default::default();
    instance.get_physical_device_surface_capabilities_khr
        (physical_device, surface, &mut caps as *mut _)
        .check().unwrap();

    impl_print_bits!(print_transform_bits(vk::SurfaceTransformFlagsKhr) {
        vk::SurfaceTransformFlagsKhr::IDENTITY_BIT_KHR => "IDENTITY",
        vk::SurfaceTransformFlagsKhr::ROTATE_90_BIT_KHR => "ROTATE_90",
        vk::SurfaceTransformFlagsKhr::ROTATE_180_BIT_KHR => "ROTATE_180",
        vk::SurfaceTransformFlagsKhr::ROTATE_270_BIT_KHR => "ROTATE_270",
        vk::SurfaceTransformFlagsKhr::HORIZONTAL_MIRROR_BIT_KHR =>
            "HORIZONTAL_MIRROR",
        vk::SurfaceTransformFlagsKhr::HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR =>
            "HORIZONTAL_MIRROR_ROTATE_90",
        vk::SurfaceTransformFlagsKhr::HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR =>
            "HORIZONTAL_MIRROR_ROTATE_180",
        vk::SurfaceTransformFlagsKhr::HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR =>
            "HORIZONTAL_MIRROR_ROTATE_270",
        vk::SurfaceTransformFlagsKhr::INHERIT_BIT_KHR => "INHERIT",
    });
    impl_print_bits!(print_composite_alpha_bits(vk::CompositeAlphaFlagsKhr) {
        vk::CompositeAlphaFlagsKhr::OPAQUE_BIT_KHR => "OPAQUE",
        vk::CompositeAlphaFlagsKhr::PRE_MULTIPLIED_BIT_KHR =>
            "PRE_MULTIPLIED",
        vk::CompositeAlphaFlagsKhr::POST_MULTIPLIED_BIT_KHR =>
            "POST_MULTIPLIED",
        vk::CompositeAlphaFlagsKhr::INHERIT_BIT_KHR => "INHERIT",
    });
    impl_print_bits!(print_image_usage_bits(vk::ImageUsageFlags) {
        vk::ImageUsageFlags::TRANSFER_SRC_BIT => "TRANSFER_SRC",
        vk::ImageUsageFlags::TRANSFER_DST_BIT => "TRANSFER_DST",
        vk::ImageUsageFlags::SAMPLED_BIT => "SAMPLED",
        vk::ImageUsageFlags::STORAGE_BIT => "STORAGE",
        vk::ImageUsageFlags::COLOR_ATTACHMENT_BIT => "COLOR_ATTACHMENT",
        vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT_BIT =>
            "DEPTH_STENCIL_ATTACHMENT",
        vk::ImageUsageFlags::TRANSIENT_ATTACHMENT_BIT =>
            "TRANSIENT_ATTACHMENT",
        vk::ImageUsageFlags::INPUT_ATTACHMENT_BIT => "INPUT_ATTACHMENT",
        vk::ImageUsageFlags::SHADING_RATE_IMAGE_BIT_NV =>
            "SHADING_RATE_IMAGE_BIT",
    });

    println!("min_image_count: {}", caps.min_image_count);
    println!("max_image_count: {}", caps.max_image_count);
    println!("current_extent: {:?}", <(u32, u32)>::from(caps.current_extent));
    println!("min_image_extent: {:?}",
        <(u32, u32)>::from(caps.min_image_extent));
    println!("max_image_extent: {:?}",
        <(u32, u32)>::from(caps.max_image_extent));
    println!("max_image_array_layers: {:?}", caps.max_image_array_layers);
    print!("supported_transforms: ");
    print_transform_bits(caps.supported_transforms);
    println!();
    print!("current_transform: ");
    print_transform_bits(caps.current_transform);
    println!();
    print!("supported_composite_alpha: ");
    print_composite_alpha_bits(caps.supported_composite_alpha);
    println!();
    print!("supported_usage_flags: ");
    print_image_usage_bits(caps.supported_usage_flags);
    println!();

    let present_modes = vk::enumerate2!(
        instance,
        get_physical_device_surface_present_modes_khr,
        physical_device,
        surface,
    ).unwrap();
    println!("present_modes:");
    for mode in present_modes.into_iter() {
        println!("  - {}", match mode {
            vk::PresentModeKhr::IMMEDIATE_KHR => "IMMEDIATE",
            vk::PresentModeKhr::MAILBOX_KHR => "MAILBOX",
            vk::PresentModeKhr::FIFO_KHR => "FIFO",
            vk::PresentModeKhr::FIFO_RELAXED_KHR => "FIFO_RELAXED",
            vk::PresentModeKhr::SHARED_DEMAND_REFRESH_KHR =>
                "SHARED_DEMAND_REFRESH",
            vk::PresentModeKhr::SHARED_CONTINUOUS_REFRESH_KHR =>
                "SHARED_CONTINUOUS_REFRESH",
            _ => continue,
        });
    }

    let formats = vk::enumerate2!(
        instance,
        get_physical_device_surface_formats_khr,
        physical_device,
        surface,
    ).unwrap();

    println!("surface_formats:");
    for fmt in formats.into_iter() {
        // Formats are too numerous to explicitly list here
        println!("  - format: {:?}", fmt.format);
        println!("    color_space: {:?}", fmt.color_space);
    }

    // A complete application would move on to create a swapchain, a
    // framebuffer, and use clear, draw, and synchronization commands to
    // render to the window.

    // Clean up (if we didn't already panic, of course)
    device.destroy_device(ptr::null());
    instance.destroy_surface_khr(surface, ptr::null());
    instance.destroy_instance(ptr::null());
    glfw::terminate();
}
