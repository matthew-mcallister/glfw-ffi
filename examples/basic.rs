extern crate glfw_ffi as glfw;

fn main() {
    unsafe { unsafe_main() }
}

unsafe fn unsafe_main() {
    if glfw::init() != glfw::TRUE {
        panic!("failed to initialize GLFW");
    }

    let window = glfw::create_window(
        640,
        480,
        b"Hello World\0".as_ptr() as *const _,
        0 as *mut _,
        0 as *mut _,
    );
    if window.is_null() {
        glfw::terminate();
        panic!("failed to create window");
    }

    while glfw::window_should_close(window) != glfw::TRUE {
        // <do rendering here>
        glfw::swap_buffers(window);
        glfw::poll_events();
    }

    glfw::terminate();
}
