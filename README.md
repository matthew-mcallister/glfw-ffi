# glfw-ffi

This project provides direct bindings to the GLFW library. Vulkan and
OpenGL are supported for rendering with no dependencies required.

Future development may yield a high-level wrapper companion library.
